import React, { useState, useEffect } from "react";
import { Form, Field } from "react-final-form";
import { OnChange } from "react-final-form-listeners";
import { getCities } from "./components/cities";
import { $config, updateConfig } from "./components/config";
import { useStore } from "effector-react";
const onSubmit = () => {
  console.log("submit");
};
const validate = () => {
  console.log("validate");
};

const getDate = async ({ id, setDates }) => {
  const { success, data } = await fetch(
    `https://www.mocky.io/v2/${id}?mocky-delay=700ms`
  ).then(x => x.json());
  if (success) {
    const dates = [];
    for (const date in data) {
      dates.push(date);
    }
    const needArr = dates.map(x => ({
      date: x,
      freeTime: (() => {
        const timeArr = [];
        for (const key in data[x]) {
          if (!data[x][key].is_not_free) timeArr.push(data[x][key]);
        }
        return timeArr;
      })()
    }));
    setDates(needArr);
  } else {
    alert("что-то пошло не так :(");
  }
};

function App() {
  const [loading, setLoading] = useState(true);
  const [dates, setDates] = useState();
  const [cities, setCities] = useState();
  const [currentCity, setCurrentCity] = useState();
  const [currentDate, setCurrentDate] = useState();
  const config = useStore($config);
  useEffect(() => {
    getCities({ setCities, setCurrentCity, setLoading, setDates, getDate });
  }, []);
  useEffect(() => {
    if (!currentDate) setCurrentDate();
  }, [currentDate]);

  return (
    <div className="App">
      {loading ? (
        "loading..."
      ) : (
        <Form
          onSubmit={onSubmit}
          render={({ handleSubmit, values }) => (
            <form onSubmit={handleSubmit}>
              <h2>Онлайн запись</h2>
              <Field name="city" component="select">
                {cities &&
                  cities.map((x, i) => (
                    <option value={x.id} key={i}>
                      {x.name}
                    </option>
                  ))}
              </Field>
              <div>
                <div>{currentCity.address}</div>
                <div>
                  {currentCity.phones.map((x, i) => (
                    <span key={i}>
                      <a href={`tel:${x}`}>{x}</a>
                      {currentCity.phones.length - 1 > i && ", "}
                    </span>
                  ))}
                </div>
                <div>Стоимость услуги {currentCity.price}</div>
              </div>
              <Field name="date" component="select">
                <option>Дата</option>
                {dates &&
                  dates.map((x, i) => <option key={i}>{x.date}</option>)}
              </Field>
              <Field name="time" component="select">
                <option>Время</option>
              </Field>
              <Field name="name">
                {({ input, meta }) => (
                  <div>
                    <input type="text" {...input} placeholder="Ваше имя" />
                    {meta.touched && meta.error && <span>{meta.error}</span>}
                  </div>
                )}
              </Field>
              <Field name="phone">
                {({ input, meta }) => (
                  <div>
                    <input
                      type="text"
                      {...input}
                      placeholder="+7(___)___-__-__"
                    />
                    {meta.touched && meta.error && <span>{meta.error}</span>}
                  </div>
                )}
              </Field>
              <OnChange name="city">
                {(value, previous) => {
                  console.log('OnChange name="city"', { value, previous });
                  setCurrentCity(cities.find(x => x.id === value));
                }}
              </OnChange>
              <OnChange name="date">
                {(value, previous) => {
                  // do something
                  console.log({ value, previous });
                  updateConfig("chk");
                  // setCurrentCity(cities.find(x => x.id === value));
                }}
              </OnChange>
              <button type="submit">Submit</button>
              {/* <pre>{JSON.stringify(cities, 2, 2)}</pre> */}
              <pre>{JSON.stringify(values, 2, 2)}</pre>
            </form>
          )}
        />
      )}

      {/* <h3>All</h3> */}
      {/* <pre>{JSON.stringify(cities, 2, 2)}</pre> */}
    </div>
  );
}

export default App;
