import { createEvent, createStore, createEffect } from "effector";

const initConfig = {
  city: "5b3480ee3200009f28d1e421",
  date: "2018-06-28",
  time: "09:00:00"
};

const setConfig = createEvent();
const $config = createStore(null);

$config.on(setConfig, (_, x) => x);

const updateConfig = createEffect().use(c => {
  setConfig(c);
});

$config.watch((a, b) => {
  if (b == undefined) {
    updateConfig(initConfig);
  } else {
    updateConfig(a);
  }
});

export { $config, updateConfig };
