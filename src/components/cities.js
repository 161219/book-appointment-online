import { createStore, createEffect } from "effector";

const $cities = createStore([]);
$cities.watch(() => {
  // alert();
});

const getCities = createEffect().use(
  async ({ setCities, setCurrentCity, setLoading, setDates, getDate }) => {
    try {
      const { cities } = await fetch(
        "https://www.mocky.io/v2/5b34c0d82f00007400376066?mocky-delay=700ms"
      ).then(x => x.json());
      if (cities) {
        setCities(cities);
        setCurrentCity(cities.find(x => (x.id = "5b3480ee3200009f28d1e421")));
        getDate({ id: "5b3480ee3200009f28d1e421", setDates });
        setLoading(false);
      } else {
        alert("Что-то не так c ответом :(");
      }
    } catch (error) {
      alert("Что-то не так с запросом:(");
      console.log(error);
    }
  }
);

export { getCities };
